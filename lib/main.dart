import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:salesmanager_home/src/bloc/authentication_bloc.dart';
import 'package:salesmanager_home/src/common/loading_indicator.dart';
import 'package:salesmanager_home/src/constants/constants_page.dart';
import 'package:salesmanager_home/src/repository/user_repositor.dart';
import 'package:salesmanager_home/src/screens/login_page.dart';
import 'package:salesmanager_home/src/screens/main_page.dart';
import 'package:salesmanager_home/src/screens/splash_page_page.dart';
import 'package:bloc/bloc.dart';
import 'package:salesmanager_home/src/states/authenticate_states.dart';

class SimpleBlocDelegate extends BlocDelegate {
  @override
  void onEvent(Bloc bloc, Object event) {
    // TODO: implement onEvent
    super.onEvent(bloc, event);
    print(event);
  }

  @override
  void onTransition(Bloc bloc, Transition transition) {
    // TODO: implement onTransition
    super.onTransition(bloc, transition);
    print(transition);
  }

  @override
  void onError(Bloc bloc, Object error, StackTrace stacktrace) {
    // TODO: implement onError
    super.onError(bloc, error, stacktrace);
    print(error);
  }
}

void main() {
  BlocSupervisor.delegate = SimpleBlocDelegate();
  final userRepository = UserRepository();
  runApp(
    BlocProvider<AuthenticationBloc>(
      builder: (context) {
        return AuthenticationBloc(userRepository: userRepository)
          ..dispatch(AppStarted());
      },
      child: SalesmanagerHome(userRepository: userRepository),
    ),
  );
}

class SalesmanagerHome extends StatelessWidget {
  final UserRepository userRepository;

  SalesmanagerHome({Key key, @required this.userRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations(
        [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
    return MaterialApp(
//      debugShowCheckedModeBanner : false,
////      title: 'Salesmanager Home',
////      theme: ThemeData(primaryColor: Colors.orange[200]),
////      routes: <String, WidgetBuilder>{
////        LOGIN: (BuildContext context) => LoginPage(),
////        MAIN_UI:(BuildContext context) => HomePage(),
////        SPLASH_SCREEN : (BuildContext context) => AnimatedSplashScreen(),
////      },
////
////      initialRoute: SPLASH_SCREEN,
      home: BlocBuilder<AuthenticationBloc, AuthenticationState>(
        builder: (context, state) {
          if (state is AuthenticationAuthenticated) {
            return HomePage();
          }
          if (state is AuthenticationUnauthenticated) {
            return LoginPage(userRepository: userRepository);
          }
          if (state is AuthenticationLoading) {
            return LoadingIndicator();
          }
          else return AnimatedSplashScreen();
        },
      ),

      theme: ThemeData(primaryColor: Colors.orange[200]),
      routes: <String, WidgetBuilder>{
        LOGIN: (BuildContext context) => LoginPage(),
        MAIN_UI:(BuildContext context) => HomePage(),
        SPLASH_SCREEN : (BuildContext context) => AnimatedSplashScreen(),
      },

      initialRoute: SPLASH_SCREEN,
    );
  }
}
