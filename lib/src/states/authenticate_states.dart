import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';


/// Class for Bloc authentication.
abstract class AuthenticationState extends Equatable{}

class AuthenticationUninitialized extends AuthenticationState{
  @override
  String toString () => 'AuthenticationUninitialized';
}

class AuthenticationAuthenticated extends AuthenticationState{
  @override
  String toString()=>'AuthenticationAuthenticated';

}

class AuthenticationUnauthenticated extends AuthenticationState{
  @override
  String toString ()=> 'AuthenticationUnauthenticated';
}

class AuthenticationLoading extends AuthenticationState{
  @override
  String toString() => 'AuthenticationLoading';
}

abstract class AuthenticationEvent extends Equatable{
  AuthenticationEvent([List props = const[]]) : super(props);

}


/// Set event for project _ Event authentication when login
class AppStarted extends AuthenticationEvent{
  @override
  String toString() => 'AppStarted';
}
class LoggedIn extends AuthenticationEvent{
  final String token;
  LoggedIn({@required this.token}) : super([token]);
  @override
  String toString()=> 'LoggedIn {token : $token}';
}

class LoggedOut extends AuthenticationEvent{
  @override
  String toString ()=> 'LoggedOut';
}