import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';


/// Class for Bloc of Login
abstract class LoginState extends Equatable {
  LoginState([List props = const[]]) : super(props);

}

class LoginInitial extends LoginState{
  @override
  String toString () => 'LoginInitial';
}

class LoginLoading extends LoginState {
  @override
  String toString ()=> 'LoginLoading';
}

class LoginFailure extends LoginState{
  final String error;
  LoginFailure ({@required this.error}) : super([error]);

  @override
  String toString()=> 'LoginFailure {error : $error}';
}

/// Class for Event of Login for App
abstract class LoginEvent extends Equatable{
  LoginEvent([List props = const[]]) : super(props);
}

class LoginButtonPressed extends LoginEvent {
  final String username;
  final String password;

  LoginButtonPressed ({@required this.username, @required this.password}) : super([username,password]);

  @override
  String toString ()=> 'LoginButtonPressed { username: $username, password: $password }';
}
