import 'package:flutter/cupertino.dart';

class UserRepository {
  Future<String> authenticate({
    @required String username,
    @required String passwword,
  }) async {
    await Future.delayed(Duration(seconds: 1));
    return 'token';
  }


  Future<void> deleteToken () async{
    /// delete from keystore / keychain
    await Future.delayed(Duration(seconds: 1));
  }

  Future<void> persistToken(String token) async{
    /// write to keystore/ keychain
    await Future.delayed(Duration(seconds: 1));

  }

  Future<bool> hasToken() async {
    /// read from keystore / keychain
    await Future.delayed(Duration(seconds: 1));
    return false;
  }
}
