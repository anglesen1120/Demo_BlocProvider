import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:salesmanager_home/src/bloc/authentication_bloc.dart';
import 'package:salesmanager_home/src/bloc/login_bloc.dart';
import 'package:salesmanager_home/src/repository/user_repositor.dart';

import 'package:salesmanager_home/src/screens/main_page.dart';
import 'package:salesmanager_home/src/screens/screen_form/login_form.dart';
import 'package:salesmanager_home/src/screens/widget/custom_waveclipper_page.dart';
import 'package:salesmanager_home/src/states/login_states.dart';

class LoginPage extends StatelessWidget {
  final UserRepository userRepository;

  LoginPage({Key key, @required this.userRepository}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
//        body: MultiBlocProvider(
//          providers: [
//            BlocProvider<AuthenticationBloc>(
//              builder: (BuildContext context) => AuthenticationBloc(),
//            ),
//            BlocProvider<LoginBloc>(
//              builder: (BuildContext context) =>
//                  LoginBloc(
//                    authentiocationBloc: BlocProvider.of<AuthenticationBloc>(
//                        context),
//                    userRepository: userRepository,
//                  ),
//            )
//          ],
//
//
//          child: LoginForm(),)
      body: BlocProvider(
        builder: (context) {
          return LoginBloc(
            authentiocationBloc: BlocProvider.of<AuthenticationBloc>(context),
            userRepository: userRepository,
          );
        },
        child: LoginForm(),
      ),
    );
  }
}
