import 'dart:async';
import 'package:meta/meta.dart';
import 'package:bloc/bloc.dart';
import 'package:salesmanager_home/src/bloc/authentication_bloc.dart';
import 'package:salesmanager_home/src/repository/user_repositor.dart';
import 'package:salesmanager_home/src/states/authenticate_states.dart';
import 'package:salesmanager_home/src/states/login_states.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
  final UserRepository userRepository;
  final AuthenticationBloc authentiocationBloc;

  LoginBloc({@required this.userRepository, @required this.authentiocationBloc});
//      : assert(userRepository != null),
//        assert(authentiocationBloc != null);

  LoginState get initialState => LoginInitial();

  @override
  Stream<LoginState> mapEventToState(LoginEvent event) async* {
    if (event is LoginButtonPressed) {
      yield LoginLoading();
      try {
        final token = await userRepository.authenticate(
          username: event.username,
          passwword: event.password,
        );
        authentiocationBloc.dispatch(LoggedIn(token: token));
        yield LoginInitial();
      } catch (error) {
        yield LoginFailure(error: error.toString());
      }
    }
  }
}
